---
title: "História"
date: 2021-09-18T23:28:40-03:00
draft: false
---

A National Basketball Association (NBA) é uma das principais ligas esportivas dos Estados Unidos e uma das mais renomadas ligas de basquete do mundo. Sua história remonta ao início do século XX, quando o basquete começou a ganhar popularidade nos Estados Unidos. No entanto, a NBA como a conhecemos hoje foi oficialmente fundada em 6 de junho de 1946, como Basketball Association of America (BAA).

Em 1949, a BAA se fundiu com a National Basketball League (NBL), formando a NBA. Desde então, a liga cresceu exponencialmente em popularidade e influência global. Times icônicos, como o Boston Celtics, Los Angeles Lakers, Chicago Bulls e Golden State Warriors, têm dominado diversas épocas da NBA, conquistando múltiplos campeonatos.

A NBA também testemunhou a ascensão de lendas do basquete, como Michael Jordan, Magic Johnson, Larry Bird, Kareem Abdul-Jabbar, LeBron James e muitos outros, que deixaram marcas indeléveis na história do esporte.

Além disso, a NBA desempenhou um papel importante na promoção da diversidade e inclusão no esporte, permitindo que jogadores de diferentes origens étnicas e culturais brilhem no cenário mundial.

A liga também enfrentou desafios ao longo de sua história, incluindo questões relacionadas a drogas, conflitos trabalhistas e disputas políticas. No entanto, a NBA continua a prosperar como uma liga globalmente reconhecida, com fãs de todas as idades e nacionalidades acompanhando seus jogos, tornando-se um verdadeiro fenômeno cultural e esportivo.